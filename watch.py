import sys
import tweepy
import ConfigParser

def openConfig():
	try:
		with open("config.cfg") as f: pass
	except IOError as e:
		print 'Can\'t read config. Config file doesn\'t exist. Creating template.'
		config = createConfig()
		with open("config.cfg", "wb") as configfile:
			config.write(configfile)
		print 'Created dummy config. Please edit config before usage.'
		sys.exit(1)
		return None
	config = ConfigParser.RawConfigParser()
	config.read('config.cfg')
	return config

def createConfig():
	config = ConfigParser.RawConfigParser()
	config.add_section("Api Keys")
	config.set("Api Keys", "consumer_key", "consumer_key")
	config.set("Api Keys", "consumer_secret", "consumer_secret")
	config.set("Api Keys", "access_key", "access_key")
	config.set("Api Keys", "access_secret", "access_key")
	return config

def auth(config):
	auth = tweepy.OAuthHandler(config.get("Api Keys", "consumer_key"), config.get("Api Keys", "consumer_secret"))
	auth.set_access_token(config.get("Api Keys", "access_key"), config.get("Api Keys", "access_secret"))
	api = tweepy.API(auth)
	return auth


class CustomStreamListener(tweepy.StreamListener):
	def on_status(self, status):
		if status.user.lang == "nl":
			print "<" +status.user.screen_name + "> " + status.text.replace("\n", "")

	def on_error(self, status_code):
		print >> sys.stderr, 'Encountered error with status code:', status_code
		return True # Don't kill the stream

	def on_timeout(self):
		print >> sys.stderr, 'Timeout...'
		return True # Don't kill the sapi

sapi = tweepy.streaming.Stream(auth(openConfig()), CustomStreamListener())
sapi.filter(track=['politie', 'zeist', 'helicopter', 'sirene', 'helikopter'])